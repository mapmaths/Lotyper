# Lotyper

## ✨高光✨

1. 简便，操作肥肠简单。
2. 文件小，懂的都懂。
3. 扩展性极强。

## 操作说明

1. 首先，打开[setup.bat](setup.bat)来把Windows Script Host设置为[WScript.exe](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/wscript)（大佬们可以跳过这一步）。
2. 接着，运行[app.vbs](app.vbs)并遵循指示。

享受打字的便利吧！:D

## 词典

“Lotyper”：这个名字是“Loop Typer”（循环打字机）的缩写，读音是/lɒ'taɪpə/。
"Lotype"：本应用**操作说明**中出现的一个动词，是“Looply Type”（循环打字）的缩写，读音是/lɒ'taɪp/。

## 更新日志

**v1.0.0** *19 Feb, 2022*
1. 完全开放